package com.example.bolsadevalores2;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements Response.ErrorListener, Response.Listener {

    public static final String REQUEST_TAG = "Valores";
    private RequestQueue mQueue;
    private String url;
    private AlertDialog alert;
    RadioButton rd1;
    RadioButton rd2;
    RadioButton rd3;
    Button bt;
    TextView tx;
    String txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rd1 = (RadioButton) findViewById(R.id.radioButton);
        rd2 = (RadioButton) findViewById(R.id.radioButton2);
        rd3 = (RadioButton) findViewById(R.id.radioButton3);
        bt = (Button)findViewById(R.id.button);
        tx = (TextView)findViewById(R.id.textView);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("ERRO");
        alert = builder.create();
    }
    @Override
    protected void onStart() {
        super.onStart();
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rd1.isChecked()) {
                    txt = "GRND3";
                } else if (rd2.isChecked()) {
                    txt = "BBAS3";
                } else {
                    txt = "TELB4";
                }
                makeRequest();
            }
        });
    }

    public void makeRequest(){
        mQueue = CustomVolleyRequestQueue.getInstance(this.getApplicationContext()).
                getRequestQueue();
        url = "http://abrawerman.com/ale/stock.php?stock="+
                txt;
        final CustomJSONObjectRequest jsonRequest = new CustomJSONObjectRequest(Request.Method.POST,
                url,new JSONObject(), this, this);
        jsonRequest.setTag(REQUEST_TAG);
        mQueue.add(jsonRequest);
    }

    @Override
    protected void onStop(){
        super.onStop();
        if(mQueue != null){
            mQueue.cancelAll(REQUEST_TAG);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(Object response) {
        String res = null;
        try {
            res = ((JSONObject)response).getString(txt);
//            tx.setText(res);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(res.equals("3,78")||res.equals("15,70")||res.equals("23,5")){
            tx.setText(res);
        }
        else{
            alert.setMessage("Não foi possível pegar o valor!");
            alert.show();
        }
    }
}
